import RemoteObject from "../remote-object"
import ByteBuffer from "../util/buffer"
import { MessageType } from "../skeleton/message-types"
import marshall from "../marshall/marshalling"
import { typeOf, DataType } from "../marshall/common"
import unmarshall from "../marshall/unmarshalling"
import { RawType } from "../marshall/common"
import DatagramHandler from "../network/datagram-handler"
import RemoteError from "../remote-error"
import PromiseState from "./promise-state"
import RequestRepeater from "./request-repeater"
import { LOGGER } from "../util/logger"


/**
 * Object allowing to call remote procedures on remote objects. For each of its functionction,
 * you should provide a remote object, a method or property id, and the arguments.
 *
 * The thread is locked until the answer is received, in case of failure (network or remote), a
 * remote exception is thrown.
 */
class ProxyClass {
    private promises: { [code: number]: PromiseState } = {}

    /**
     * Call the setter of a remote object property
     */
    set<T> (obj: RemoteObject, propertyId: number, value: T): Promise<void> {
        const content = marshall(value)

        return this.syncCall(
            this.buildMessage(
                obj,
                MessageType.SET,
                ByteBuffer.concat(
                    ByteBuffer.alloc(4).putInt(propertyId),
                    content
                )
            ),
            obj
        )
            .then(result => {
                if (result == null || !unmarshall(result, typeOf(RawType.BOOLEAN))) {
                    throw new RemoteError('RemoteError', `unable to call setter`);
                }
            });
    }

    /**
     * Calls the getter of a remote object property
     */
    get<T>(obj: RemoteObject, propertyId: number, expectedReturn: DataType): Promise<T> {
        return this.syncCall(
            this.buildMessage(
                obj,
                MessageType.GET,
                ByteBuffer.alloc(4).putInt(propertyId)
            ),
            obj
        )
            .then(it => unmarshall<T>(it!, expectedReturn));
    }

    /**
     * Call a method of the remote object, and return the result of the given type
     */
    call<T>(obj: RemoteObject, methodId: number, args: Array<any>, expectedReturn: DataType): Promise<T> {
        const marshalledArgs = ByteBuffer.concat(
            ByteBuffer.alloc(4).putInt(methodId),
            ...args.map(it => marshall(it))
        );

        return this.syncCall(
            this.buildMessage(
                obj,
                MessageType.CALL,
                marshalledArgs
            ),
            obj
        )
            .then(it => unmarshall(it!, expectedReturn));
    }

    /**
     * Build a message to send to the remote server, containing the remote object identifier, the message type code,
     * and the data content (arguments)
     */
    private buildMessage(remoteObject: RemoteObject, type: number, dataContent: ByteBuffer): ByteBuffer {
        return ByteBuffer.concat(
           ByteBuffer.alloc(1).putByte(type),
           marshall(remoteObject),
           dataContent
        );
    }

    /**
     * Call asynchronously a remote call procedure (either get, set or functionction), returns the byte buffer obtained with
     * the result
     */
    private async syncCall(buffer: ByteBuffer, remoteObject: RemoteObject): Promise<ByteBuffer | null> {
        const callId = DatagramHandler.sendNew(buffer, remoteObject.identifier.ip, remoteObject.identifier.port)

        return new Promise<ByteBuffer | null>((resolve, reject) => {
            this.promises[callId] = new PromiseState(resolve, reject);

            // Launch timeout process, to re-send data in case the server does not answer
            new RequestRepeater(callId, remoteObject, buffer, this.promises[callId]);
        })
    }

    /**
     * Resume a thread locked by a remote method invocation when the result is here or an error is thrown
     */
    unlock(callId: number, result: ByteBuffer | null, error: boolean = false) {
        if (callId in this.promises) {
            const promise = this.promises[callId];

            if (!error) {
                promise.resolve(result)
            } else {
                promise.reject(new RemoteError(
                    unmarshall(result!, typeOf(RawType.STRING)),
                    unmarshall(result!, typeOf(RawType.STRING))
                ))
            }

            delete this.promises[callId];
        } else {
            LOGGER.error("received packet for unknown call id " + callId)
        }
    }
}

export const Proxy = new ProxyClass();