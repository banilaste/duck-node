

export default class RemoteError extends Error {
    constructor(exceptionClass: string, message: string) {
        super(`${exceptionClass}: ${message}`);
    }
}
