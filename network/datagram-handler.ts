
import { Socket, RemoteInfo, createSocket } from "dgram";
import duckProperties from "../properties";
import { LOGGER } from "../util/logger";
import ByteBuffer from "../util/buffer";
import Request from "./request";
import Skeleton from "../skeleton/skeleton";
import DatagramFilter from "./datagram-filter";

// Content settings
const BUFFER_SIZE = 25565
// 4 for int callId, 4 for int attempt, 4 for int packetNumber, 1 for last packet boolean
const HEADER_SIZE = 13


class DatagramHandlerClass {
    private socket: Socket | null = null
    private messages: { [key: string]: Request } = {}
    private callIndex = Date.now() % 1147483648 // number related to current timestamp (holdable on 4 bytes)
    public filters: DatagramFilter[] = []

    init() {
        // Create socket using the provided factory
        this.socket = createSocket('udp4');
        this.socket.bind(duckProperties.localPort);
        
        this.socket.on('message', this.handleDatagram.bind(this));
    }


    private async handleDatagram(datagram: Buffer, { address, port, size }: RemoteInfo) {
        const buffer = ByteBuffer.of(datagram);

        // Read content from buffer
        const callId = buffer.int()
        const attempt = buffer.int()
        const packetNumber = buffer.int()
        const isLast = buffer.get() != 0
        const id = `${callId}${address}${port}-${attempt}`

        const data = ByteBuffer.alloc(size - HEADER_SIZE) // content - 9 octet for headers
        buffer.buffer.copy(data.buffer, 0, HEADER_SIZE)

        LOGGER.debug(`[${address}:${port}] received packet, callId = ${callId}, attempt = ${attempt}, ` +
            `packetNumber = ${packetNumber}, last = ${isLast}, data = {${data.buffer.join(", ")}}`)

        // Retrieve of create message
        if (!(id in this.messages)) {
            this.messages[id] = new Request()
        }
        const message = this.messages[id]

        // Apply content to data
        message.put(packetNumber, data, isLast)

        // Trigger method
        if (message.done()) {
            message.triggered = Date.now()

            // Remove messages from cache
            delete this.messages[id]

            // Compute request
            const dataResult = await Skeleton.handle(callId, `${callId}${address}${port}`, message.compile())

            // Send result
            if (dataResult != null) {
                this.send(callId, dataResult, address, port, attempt)
            }
        }
    }

    /**
     * Send new request, generate a request id and return it
     */
    sendNew(content: ByteBuffer, destination: string, port: number): number {
        return this.send(this.callIndex++, content, destination, port, 0);
    }

    /**
     * Sand answer to existing request
     */
    send(callId: number = -1, content: ByteBuffer, destination: string, port: number, attempt: number): number {
        const packets = Math.floor(content.buffer.length / (BUFFER_SIZE - HEADER_SIZE) + 1);

        for (let it = 0; it < packets; it++) {
            const last = packets == it + 1
            const size = last
                ? content.buffer.length - (BUFFER_SIZE - HEADER_SIZE) * it
                : BUFFER_SIZE - HEADER_SIZE;

            const slicedContent = content.buffer.subarray(
                (BUFFER_SIZE - HEADER_SIZE) * it,
                (BUFFER_SIZE - HEADER_SIZE) * it + size
            )

            const packet = Buffer.concat([
                ByteBuffer.alloc(HEADER_SIZE)
                    .putInt(callId)
                    .putInt(attempt)
                    .putInt(it)
                    .putByte(last ? 1 : 0)
                    .buffer,
                slicedContent
            ]);


            LOGGER.debug(`[${destination}:${port}] sending packet, callId = ${callId}, attempt = ${attempt}, packetNumber = ${it}, last = ${last}, data = {${slicedContent.join(", ")}}`)
            
            // Apply filters
            if (this.filters.every(filter => filter(packet, destination, port))) {
                this.socket?.send(packet, 0, size + HEADER_SIZE, port, destination);
            }
        }

        return callId
    }
}


const DatagramHandler = new DatagramHandlerClass();
export default DatagramHandler;