import ByteBuffer from "../util/buffer"


export default class Request {
    constructor(
        public readonly data: (ByteBuffer | null)[] = [],
        public last: number = -1,
        public triggered: number = -1
    ) {}
    /**
     * Check if all the data is received
     */
    done(): boolean {
        return this.last != -1 && this.data.length - 1 >= this.last && this.data.every(it => it != null);
    }

    /**
     * Regroup all the data into a single buffer
     */
    compile(): ByteBuffer {
        return ByteBuffer.concat(...this.data as ByteBuffer[])
    }

    put(packetNumber: number, packet: ByteBuffer, last: boolean) {
        // Extends array with nulls
        while (packetNumber >= this.data.length) {
            this.data[this.data.length] = null;
        }

        this.data[packetNumber] = packet
        if (last) {
            this.last = packetNumber
        }
    }
}