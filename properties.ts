/**
 * Properties of the datagram server
 */
export default {
    localAddress: new Uint8Array([127, 0, 0, 1]),
    localPort: 22333
}
