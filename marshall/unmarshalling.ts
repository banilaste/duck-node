import ByteBuffer from "../util/buffer"
import { NULL, MarshallingError, ESCAPE, DataType, RawType, typeOf } from "./common"
import RemoteObjectIdentifier from "../remote-object-identifier"
import RemoteObject from "../remote-object"
import { DataClassFields } from "../annotations"

function unmarshallAny(buffer: ByteBuffer, kType: DataType, notEscaped: boolean = true): any {
    // Handle null
    if (notEscaped && kType.type != RawType.UNDEFINED && buffer.peek() == NULL) {
        buffer.get()

        if (!kType.nullable) {
            throw new MarshallingError(`null received for non null type`)
        }

        return null
    }

	// Skip escape character
    if (notEscaped && kType.type != RawType.UNDEFINED && buffer.peek() == ESCAPE) {
        buffer.get()
    }

	// Array case
	if (kType.arrayDepth > 0) {
		return Array(buffer.int())
			.fill(0)
			.map(_ => unmarshall(
				buffer, 
				{
					type: kType.type,
					nullable: kType.nullable,
					arrayDepth: kType.arrayDepth - 1
				}
			))
	}

	// Other cases
	switch(kType.type) {
		case RawType.STRING:
			const strBytes = Buffer.alloc(buffer.int());
			buffer.pipeInto(strBytes);
			return strBytes.toString("utf-8")

		case RawType.LONG:
			return buffer.long();

		case RawType.INT:
			return buffer.int();

		case RawType.FLOAT:
			return buffer.float();

		case RawType.DOUBLE:
			return buffer.double();

		case RawType.UNDEFINED:
			return undefined;
			

		case RawType.BYTES:
			const bytes = Buffer.alloc(buffer.int())
			buffer.pipeInto(bytes);
			return bytes
		
		default:
			// Constructor of something ?
			if (kType.type instanceof Function) {
				const typeAny = kType.type as any;

				// Remote object
				if (kType.type.prototype instanceof RemoteObject || kType.type == RemoteObject) {
					return RemoteObjectIdentifier.ofBytes(
						buffer.int(),
						unmarshall(buffer, typeOf(RawType.BYTES)),
						buffer.int()
					)
				}
				
				// Defined data class
				else if (typeAny.isDataClass) {
					const data = typeAny.dataFields as DataClassFields;

					// Marshall from names and call constructor
					return new typeAny(
						...data.sort((a, b) => a.id - b.id)
							.map(it => unmarshall(
								buffer,
								it.type
							))
					)
				}
			}
			
			throw new MarshallingError(`unhandled type ${kType}`)
	}
}

/**
 * Given the provided type, unmarshall result
 * @param buffer buffer to be read from
 * @param kType type description of the resulting data 
 * @param notEscaped whether or not the escape character should be considered
 */
export default function unmarshall<T>(buffer: ByteBuffer, kType: DataType, notEscaped: boolean = true): T {
	return unmarshallAny(buffer, kType, notEscaped) as T
}