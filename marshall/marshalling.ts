import ByteBuffer from "../util/buffer"
import RemoteObject from "../remote-object";
import { MarshallingError, NULL, ESCAPE } from "./common";
import { RawType } from "..";
import { DataClassFields } from "../annotations";

function marshallNumber(num: number, numberType: RawType | Function) {
    switch (numberType) {
        case RawType.LONG:
            return ByteBuffer.alloc(8).putLong(BigInt(num));
        case RawType.INT:
            return ByteBuffer.alloc(4).putInt(num);
        case RawType.FLOAT:
            return ByteBuffer.alloc(4).putFloat(num);
        case RawType.DOUBLE:
            return ByteBuffer.alloc(8).putDouble(num);
        default:
            throw new Error("can only marshallNumber() on number raw types")
    }
}

function marshallObject(obj: object, numberType: RawType | Function = RawType.INT) {
    if (obj instanceof Array) {
        const objects = obj.map(it => marshall(it, numberType));

        return ByteBuffer.concat(
            ByteBuffer.alloc(4).putInt(objects.length),
            ...objects
        )
    } else if (obj instanceof ByteBuffer || obj instanceof Uint8Array || obj instanceof Buffer) {
        return ByteBuffer.concat(
            ByteBuffer.alloc(4).putInt(
                obj instanceof ByteBuffer ? obj.buffer.length : obj.length
            ),
            obj
        )
    } else if (obj instanceof RemoteObject) {
        return ByteBuffer.concat(
            marshallNumber(obj.identifier.code, RawType.INT),
            marshall(obj.identifier.rawIp()),
            marshallNumber(obj.identifier.port, RawType.INT)
        )
    } else if ((obj.constructor as any).isDataClass) {
        const data = (obj.constructor as any).dataFields as DataClassFields;
        // Marshall from names
        return ByteBuffer.concat(
            ...data.sort((a, b) => a.id - b.id)
                .map(it => marshall(
                    (obj as any)[it.name]
                ))
        )
        
    } else {
        throw new MarshallingError(`unsupported type : ${obj.constructor.name}`)
    }
}

export default function marshall(obj: any, numberType: RawType | Function = RawType.INT, escape: boolean = true): ByteBuffer {
    if (obj == null) {
        return ByteBuffer.alloc(1).putByte(NULL)
    }

    let result;
    switch(typeof obj) {
        case "bigint":
            result = ByteBuffer.alloc(8).putLong(obj);
            break;

        case "boolean":
            result =  ByteBuffer.alloc(1).putByte(obj ? 1 : 0);
            break;

        case "number":
            result = marshallNumber(obj, numberType);
            break;

        case "undefined":
            result = ByteBuffer.alloc(0)
            break;

        case "string":
            const bytes = ByteBuffer.from(obj);
            result = ByteBuffer.concat(
                marshallNumber(bytes.buffer.length, RawType.INT),
                bytes
            );
            break;
        
        case "object":
            result = marshallObject(obj);
            break;

        default:
            throw new MarshallingError(`unsupported type : ${typeof obj}`)
    }

    // TODO generated data classes
    // TODO test escape boolean

    // Escape first byte same value as if null or escape
    if (result != null && result.buffer.length > 0 && escape && (result.buffer[0] == NULL || result.buffer[0] == ESCAPE)) {
        return ByteBuffer.concat(
            ByteBuffer.alloc(1).putByte(ESCAPE),
            result
        )
    } else {
        return result
    }
}
