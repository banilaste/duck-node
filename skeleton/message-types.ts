import MessageHandler from "./message-handler"
import RemoteObject from "../remote-object"
import { LOGGER } from "../util/logger"
import MessageAnswer from "./message-answer"
import ByteBuffer from "../util/buffer"
import marshall from "../marshall/marshalling"
import unmarshall from "../marshall/unmarshalling"
import { Proxy } from "../proxy/proxy"


export enum MessageType {
    GET = 0, SET = 1, CALL = 2, ANSWER = 3, ERROR = 4
}

/**
 * Handlers for incoming messages
 */
export const messageHandlers: MessageHandler[] = [
    // Get = 0
    async function(buffer: ByteBuffer, callId: number, localCallId: string) {
        const remoteObject = RemoteObject.fromByteBuffer(buffer);
        const methodId = buffer.int();

        const member = RemoteObject
            .interfaceData(remoteObject.constructor as new () => RemoteObject)[methodId];

        LOGGER.verbose(
            `calling getter of ${remoteObject.constructor.name}@${remoteObject.identifier.code} for ${member.method}`
        )

        return new MessageAnswer(marshall(
            (remoteObject as any)[member.method],
            member.numberType
        ));
    },
    // Set = 1
    async function(buffer: ByteBuffer, callId: number, localCallId: string) {
        // Call parameters
        const remoteObject = RemoteObject.fromByteBuffer(buffer);
        const methodId = buffer.int();

        const member = RemoteObject
            .interfaceData(remoteObject.constructor as new () => RemoteObject)[methodId];

        LOGGER.verbose(
            `calling setter of ${remoteObject.constructor.name}@${remoteObject.identifier.code}` +
                    ` for ${member.method}`
        );

        // Set value
        (remoteObject as any)[member.method] = unmarshall(
            buffer,
            member.argumentTypes[0]
        );
        
        return new MessageAnswer(marshall(true), true);
    },
    // Call = 2
    async function(buffer: ByteBuffer, callId: number, localCallId: string) {
        // Call parameters
        const remoteObject = RemoteObject.fromByteBuffer(buffer);
        const methodId = buffer.int();

        // Retrieve method
        const method = RemoteObject
            .interfaceData(remoteObject.constructor as new () => RemoteObject)[methodId];

        // Find method params
        const callParams = method.argumentTypes.map(it =>
            unmarshall(buffer, it) as any
        )


        LOGGER.verbose(
            `calling ${method.method.length} of ${remoteObject.constructor.name}@${remoteObject.identifier.code}, ` +
                    `arguments = ${callParams.join(", ")}}`
        );

        // Call params
        const result = await (remoteObject as any)[method.method].call(remoteObject, ...callParams);
        
        return new MessageAnswer(
            // Marshalled call result
            method.numberType ? marshall(result, method.numberType) : ByteBuffer.from(""),
            // Save by default unless the method is marked with @NoCache
            !method.noCaching
        )
    },

    // Answer = 3
    async function(buffer: ByteBuffer, callId: number, localCallId: string) {
        Proxy.unlock(callId, buffer)
        return null
    },

    // Error = 4
    async function (buffer: ByteBuffer, callId: number, localCallId: string) {
        Proxy.unlock(callId, buffer, true)
        return null
    }
];
