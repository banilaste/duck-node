import ByteBuffer from "../util/buffer";

/**
 * Wrapper for the answer of a request, contains a boolean to indicate whether this result
 * should be cached to be served again in case the transmission fails
 */
export default class MessageAnswer {
	constructor(public data: ByteBuffer, public save: boolean = false) {}
}