import MessageAnswer from "./message-answer";
import ByteBuffer from "../util/buffer";

/**
 * Handler for a message type
 *
 * @see MessageType for implementations
 */
type MessageHandler = (buffer: ByteBuffer, callId: number, localCallId: string) => Promise<MessageAnswer | null>;

export default MessageHandler;