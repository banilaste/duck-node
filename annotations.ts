import { DataType, RawType } from "./marshall/common";

export type RemoteInterfaceData = { [key: number]: { 
		method: string, // method or variable name
		argumentTypes: DataType[],
		numberType?: RawType  | Function,
		noCaching?: boolean
	}
};

export type DataClassFields = {
	name: string,
	type: DataType,
	id: number
}[];

/**
 * Remote interface (abstract definition of a remote object)
 */
export function RemoteInterface(data: RemoteInterfaceData) {
	return function(clazz: any)  {
		clazz.isRemoteInterface = true;
		clazz.remoteInterfaceData = data;
	}
}

/**
 * Stub for a remote object
 */
export function RemoteStub(clazz: any) {
	clazz.isRemoteStub = true;
}

/**
 * Data class
 */
export function DataClass(fields: DataClassFields) {
	return function(clazz: any)  {
		clazz.isDataClass = true;
		clazz.dataFields = fields;
	}
}