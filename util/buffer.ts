/**
 * Alias for buffer function to look like java ByteBuffer
 */
export default class ByteBuffer {
	private index = 0;

	private constructor(public buffer: Buffer) {}

	pipeInto(bytes: Buffer | Uint8Array) {
		this.buffer.copy(bytes, 0, this.index);
		this.index += bytes.length;
	}

	putLong(val: bigint) {
		this.buffer.writeBigInt64BE(val, this.index);
		this.index += 8;
		return this;
	}

	putInt(val: number) {
		this.buffer.writeInt32BE(val, this.index);
		this.index += 4;
		return this;
	}

	putFloat(val: number) {
		this.buffer.writeFloatBE(val, this.index);
		this.index += 4;
		return this;
	}

	putDouble(val: number) {
		this.buffer.writeDoubleBE(val, this.index);
		this.index += 8;
		return this;
	}

	putByte(val: number) {
		this.buffer.writeInt8(val, this.index);
		this.index += 1;
		return this;
	}

	int() {
		const res = this.buffer.readInt32BE(this.index);
		this.index += 4;
		return res;
	}

	long() {
		const res = this.buffer.readBigInt64BE(this.index);
		this.index += 8;
		return res;
	}

	float() {
		const res = this.buffer.readFloatBE(this.index);
		this.index += 4;
		return res;
	}

	double() {
		const res = this.buffer.readDoubleBE(this.index);
		this.index += 8;
		return res;
	}

	get() {
		const res = this.buffer.readInt8(this.index);
		this.index += 1;
		return res;
	}

	peek() {
		return this.buffer.readInt8(this.index);
	}

	static alloc(size: number) {
		return new ByteBuffer(Buffer.allocUnsafe(size));
	}

	static concat(...buffers: (Buffer | ByteBuffer | Uint8Array)[]) {
		return new ByteBuffer(Buffer.concat(buffers.map(it => it instanceof ByteBuffer ? it.buffer : it)))
	}

    static from(obj: string) {
        return new ByteBuffer(Buffer.from(obj));
	}
	
    static of(datagram: Buffer) {
        return new ByteBuffer(datagram);
    }
}