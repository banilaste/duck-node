
export const LogLevel: { [key: string]: number } = {
    VERBOSE: 50,
    DEBUG: 100,
    WARNING: 400,
    INFO: 500,
    ERROR: 800,
    RESULT: 1000
}

/**
 * Simple logger for DUCK purpose
 */
class Logger {
    level = LogLevel.WARNING
    colorEnabled = true

    private log(level: number, prefix: string, content: any) {
        if (level >= this.level) {
            console.log(`${prefix}: ${content?.toString()}\u001B[0m`)
        }
    }

    verbose(content: any) { this.log(LogLevel.VERBOSE, (this.colorEnabled ? "\u001B[35m" : "") + "VERBOSE", content); }
    debug(content: any) { this.log(LogLevel.DEBUG, (this.colorEnabled ? "\u001B[35m" : "") + "DEBUG", content); }
    warn(content: any) { this.log(LogLevel.WARNING, (this.colorEnabled ? "\u001B[33m" : "") + "WARNING", content); }
    info(content: any) { this.log(LogLevel.INFO, "INFO", content); }
    error(content: any) { this.log(LogLevel.ERROR, (this.colorEnabled ? "\u001B[31m" : "") + "ERROR", content); }
    result(content: any) { this.log(LogLevel.RESULT, (this.colorEnabled ? "\u001B[36m" : "") + "RESULT", content); }
}

export const LOGGER = new Logger();