import { LOGGER } from "./logger";


/**
 * Parse main() arguments into a map. The format for argument is --key=value, or --key, other arguments are ignored
 */
export function parseArgs(args: Array<string>): { [name: string]: string; } {
    const enabling = /--([a-zA-Z]+)/;
    const affectation = /--([a-zA-Z]+)=([a-zA-Z0-9:._\/\\\-]+)/;

    const parsed: {[name: string]: string} = {}

    args.forEach(it => {
        // Parse every argument into the map, first affectation
        let match = it.match(affectation);
        if (match) {
            parsed[match[1]] = match[2]
        } else if (match = it.match(enabling)) {
            parsed[match[1]] = ""
        }
    });

    return parsed
}
